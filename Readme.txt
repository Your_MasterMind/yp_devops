Проект в гите состоит из 3х папок.
backend - исходный код бекенда
frontend - исходный код фронтенда
infra - папка для настройки инфраструктуры.

При любом изменении файлов бекенда, фронтенда, запускается обработчик, что собирает новую версию приложения, и помещает ее
в хранилище контейнеров для гитлаба.

При изменении в Helm Chart пельменной, запускается обработчик, что помещает новую версию helm chart пельменной в нексус https://nexus.k8s.praktikum-services.tech

Структура папки infra:
charts - helm chart для установки пельменной, прометея и графаны
ingress - папка для настройки Yandex ALB Ingress Controller
terra - конфигурация терраформа для создания k8s кластера, а так же S3 bucket 


Развертывание инфраструктуры:

Установка инфраструктуры происходит в полуавтоматическом режиме.

1.Yandex Cli
В начале на пк необходимо установить yandex cli (далее yc)
Делается это командой curl -sSL https://storage.yandexcloud.net/yandexcloud-yc/install.sh | bash
Далее, настройка профиля для работы
Команда:
yc config profile create test
yc config set folder-id <ID каталога>
yc init
Необходимые данные для настройки можно получить в своем личном кабинете Yandex Cloud.
Подробнее:
https://cloud.yandex.ru/docs/cli/operations/install-cli
https://cloud.yandex.ru/docs/cli/operations/profile/profile-create

После настройки yc, необходимо получить ключ для своего сервисного аккаунта
yc iam service-account --folder-id <ID_каталога> list
yc iam key create --service-account-name default-sa --output key.json --folder-id <ID_каталога>
yc config profile create sa-profile
yc config set service-account-key key.json
yc config set cloud-id <ID облака>
yc config set folder-id <ID каталога>
Подробнее:
https://cloud.yandex.ru/docs/cli/operations/authentication/service-account

2.Helm Chart
Для работы далее необходим менеджер пакетов helm.
Установка происходит с помощью следующих команд:
curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
sudo apt-get update
sudo apt-get install helm

Подробнее:
https://helm.sh/docs/intro/install/

3.Домен
Небходимо получить доступ к редактированию ns записей домена, что мы далее планируем использовать.
Необходимо поменять NS записи на:
ns1.yandexcloud.net
ns2.yandexcloud.net.
(Изменение данных записей занимает от 4 до 20 часов)

Далее вводим команду:

yc dns zone create \
--name yc-courses --zone <ваш домен с точкой на конце> \
--publiс-visibility

4.Terraform
Далее, переходим в папку /infra/terra/k8s, помещаем получыенный ранее ключ key.json в папку secret
И запускаем комаду
terraform plan
При отсутвии ошибок запускаем команду
terraform apply

Будет создано следующее:
- Сервисный аккаунт для работы с k8s
- 3 новых подсети для работы с кластером. [10.1.0.0/16, 10.128.0.0/24, 10.112.0.0/16]
- Правила фильтрации трафика, необходимые для работы кластера k8s
- Кластер k8s
- 2 Виртуальные машины, что будут использоваться в качестве нод k8s
- ключ для KMS
- Статический белый адрес
- Запрос на создание сертификата домена.

Далее необходимо перейти в папку bucket, где так же в папку secret поместить файл key.json
Последовательно выполнить команды terraform plan и terraform apply

Будет создано Оbject Storage Bucket, куда будут помещены необходимы для работы сайта изображения.

5.Настройка Yandex ALB Ingress Controller
Переходим в папку /infra/ingress
И там запускаем скрипт script_for_ingress

Он последовательно выполнит следующие задачи:
- Добавление  а записи в наш домен информацию о статическом белом адресе
- Добавление CNAME записи для получения сертификата
- Подключение к кластеру кубернетис
- Установка Yandex ALB Ingress Controller

6.Установка приложений.
Для установки приложений следует зайти в папку /infra/charts
Первым делом запустить скрипт script_for_values, который добавит необходимые данные для подключения к нашему кластеру.
Далее последовательно запустить команды:
helm install mrpelmen pelmen_chart/
helm install prometheus prometheus/
helm install grafana grafana/

В течение 5 минут будет создан балансировщик, и сайты будут доступны по адресам:
shop.infra.[yourdomain.name]
prom.infra.[yourdomain.name]
grafana.infra.[yourdomain.name]
