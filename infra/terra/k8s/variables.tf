variable "cloud_id" {
  description = "y.cloud ID"
  type = string
  default = "b1gvq1t90e68bfmc9kqh"
}

variable "folder_id" {
  description = "y.cloud folder ID"
  type = string
  default = "b1gs7n39pno67cjheque"
}

variable "zone" {
  description = "def zone"
  type = string
  default = "ru-central1-a"
}

variable "image_id" {
  description = "id of OS image"
  type = string
  default = "fd80qm01ah03dkqb14lc"
}
