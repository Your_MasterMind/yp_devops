locals {
  k8s_version = "1.22"
  sa_name     = "sa4k8pelmen"
  sa_ingr     = "sa4ingress"
}

resource "yandex_kubernetes_cluster" "kuber-duber" {
  name = "kuber-duber"
  network_id = yandex_vpc_network.net4k8-pelmen.id
  master {
    version = local.k8s_version
    public_ip = true
    zonal {
      zone      = yandex_vpc_subnet.net4k8-pelmen.zone
      subnet_id = yandex_vpc_subnet.net4k8-pelmen.id
    }
    security_group_ids = [yandex_vpc_security_group.k8s-beria.id]
  }
  service_account_id      = yandex_iam_service_account.sa4k8pelmen.id
  node_service_account_id = yandex_iam_service_account.sa4k8pelmen.id
  depends_on = [
    yandex_resourcemanager_folder_iam_member.k8s-clusters-agent,
    yandex_resourcemanager_folder_iam_member.vpc-public-admin,
    yandex_resourcemanager_folder_iam_member.images-puller
  ]
  kms_provider {
    key_id = yandex_kms_symmetric_key.kms-key.id
  }
}

resource "yandex_vpc_network" "net4k8-pelmen" {
  name = "net4k8-pelmen"
}

resource "yandex_vpc_subnet" "net4k8-pelmen" {
  v4_cidr_blocks = ["10.1.0.0/16"]
  zone           = "ru-central1-a"
  name = "main_sub_net4k8-pelmen"
  network_id     = yandex_vpc_network.net4k8-pelmen.id
}

resource "yandex_vpc_address" "pelmen_white" {
  name = "yandex_vpc_address.pelmen_white"

  external_ipv4_address {
    zone_id = "ru-central1-a"
  }
}

resource "yandex_iam_service_account" "sa4ingress" {
  name        = local.sa_ingr
  description = "Сервисный для Ингресса"
}

resource "yandex_resourcemanager_folder_iam_binding" "binding-ingress-editor" {
  # Сервисному аккаунту ингресса назначается роль "alb.editor".
  folder_id = var.folder_id
  role      = "alb.editor"
  members   = [ "serviceAccount:${yandex_iam_service_account.sa4ingress.id}" ]
}

resource "yandex_resourcemanager_folder_iam_binding" "binding-ingress-vpc-publicadmin" {
  # Сервисному аккаунту назначается роль "vpc-publicadmin".
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  members   = [ "serviceAccount:${yandex_iam_service_account.sa4ingress.id}" ]
}

resource "yandex_resourcemanager_folder_iam_binding" "binding-ingress-cm-certificate-downloader" {
  # Сервисному аккаунту назначается роль "vpc-publicadmin".
  folder_id = var.folder_id
  role      = "certificate-manager.certificates.downloader"
  members   = ["serviceAccount:${yandex_iam_service_account.sa4ingress.id}" ]
}

resource "yandex_resourcemanager_folder_iam_binding" "binding-ingress-compute-viewer" {
  # Сервисному аккаунту назначается роль "vpc-publicadmin".
  folder_id = var.folder_id
  role      = "compute.viewer"
  members   = [ "serviceAccount:${yandex_iam_service_account.sa4ingress.id}" ]
}

resource "yandex_iam_service_account" "sa4k8pelmen" {
  name        = local.sa_name
  description = "K8S zonal service account"
}

resource "yandex_resourcemanager_folder_iam_member" "k8s-clusters-agent" {
  # Сервисному аккаунту назначается роль "k8s.clusters.agent".
  folder_id = var.folder_id
  role      = "k8s.clusters.agent"
  member    = "serviceAccount:${yandex_iam_service_account.sa4k8pelmen.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.sa4k8pelmen.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.sa4k8pelmen.id}"
}

resource "yandex_kms_symmetric_key" "kms-key" {
  # Ключ для шифрования важной информации, такой как пароли, OAuth-токены и SSH-ключи.
  name              = "kms-key"
  default_algorithm = "AES_128"
  rotation_period   = "8760h" # 1 год.
}


resource "yandex_vpc_security_group" "k8s-beria" {
  name        = "k8s-beria"
  description = "Правила группы разрешают подключение к сервисам из интернета. Примените правила только для групп узлов."
  network_id  = yandex_vpc_network.net4k8-pelmen.id
  ingress {
    protocol          = "TCP"
    description       = "Правило разрешает проверки доступности с диапазона адресов балансировщика нагрузки. Нужно для работы отказоустойчивого кластера и сервисов балансировщика."
    predefined_target = "loadbalancer_healthchecks"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol          = "ANY"
    description       = "Правило разрешает взаимодействие мастер-узел и узел-узел внутри группы безопасности."
    predefined_target = "self_security_group"
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol          = "ANY"
    description       = "Правило разрешает взаимодействие под-под и сервис-сервис. Укажите подсети вашего кластера и сервисов."
    v4_cidr_blocks    = ["10.96.0.0/16", "10.112.0.0/16", "10.1.0.0/16"]
    from_port         = 0
    to_port           = 65535
  }
  ingress {
    protocol          = "ICMP"
    description       = "Правило разрешает отладочные ICMP-пакеты из внутренних подсетей."
    v4_cidr_blocks    = ["10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"]
  }
  ingress {
    protocol          = "TCP"
    description       = "Правило разрешает входящий трафик из интернета на диапазон портов NodePort. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks    = ["0.0.0.0/0"]
    from_port         = 30000
    to_port           = 32767
  }
  ingress {
    protocol          = "TCP"
    description       = "Правило разрешает входящий трафик из интернета http. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks    = ["0.0.0.0/0"]
    from_port         = 0
    to_port           = 80
  }
  
  ingress {
    protocol          = "TCP"
    description       = "Правило разрешает входящий трафик из интернета на https. Добавьте или измените порты на нужные вам."
    v4_cidr_blocks    = ["0.0.0.0/0"]
    from_port         = 0
    to_port           = 443
  }
  egress {
    protocol          = "ANY"
    description       = "Правило разрешает весь исходящий трафик. Узлы могут связаться с Yandex Container Registry, Yandex Object Storage, Docker Hub и т. д."
    v4_cidr_blocks    = ["0.0.0.0/0"]
    from_port         = 0
    to_port           = 65535
  }
}


resource "yandex_kubernetes_node_group" "k8s-pelmen-group" {
  name        = "k8s-pelmen-group"
  cluster_id  = yandex_kubernetes_cluster.kuber-duber.id
  version     = local.k8s_version

  scale_policy {
    auto_scale {
      min     = 2
      max     = 5
      initial = 2
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  instance_template {
    platform_id = "standard-v3"

    network_interface {
      nat                = true
      subnet_ids         = [yandex_vpc_subnet.net4k8-pelmen.id]
    }

    resources {
      memory = 4 # RAM quantity in GB
      cores  = 2 # Number of CPU cores
    }

    boot_disk {
      type = "network-hdd"
      size = 42 # Disk size in GB
    }
  }
}


resource "yandex_cm_certificate" "shop_mrpelmen_ru" {
  name    = "infra-mrpelmen"
  domains = ["*.infra.mrpelmen.ru", "infra.mrpelmen.ru"]

  managed {
    challenge_type  = "DNS_CNAME"
    challenge_count = 1
  }
}



