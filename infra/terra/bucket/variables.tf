variable "cloud_id" {
  description = "y.cloud ID"
  type = string
  default = "b1gvq1t90e68bfmc9kqh"
}

variable "folder_id" {
  description = "y.cloud folder ID"
  type = string
  default = "b1gs7n39pno67cjheque"
}

variable "zone" {
  description = "def zone"
  type = string
  default = "ru-central1-a"
}


variable image_names {
    type = list(string)
    description = "list of all image names"
    default = [1,2,3,4,5,6,7,8]
}
