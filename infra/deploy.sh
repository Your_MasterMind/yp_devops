#! /bin/bash
set -xe

pwd


export HELM_VER=$(grep version ./pelmen_helm_chart/Chart.yaml | head -n 1 | awk -F ': ' '{print $2}')
export HELM_NAME=$(grep name ./pelmen_helm_chart/Chart.yaml | head -n 1 | awk -F ': ' '{print $2}')

echo $HELM_VER

helm package pelmen_helm_chart/


helm repo add YaNexus ${HELM_ADDR} --username ${Y_USER} --password ${Y_PASS}


curl -u ${Y_USER}:${Y_PASS} ${HELM_ADDR} --upload-file ${HELM_NAME}-${HELM_VER}.tgz
